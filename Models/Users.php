<?php
namespace Models;

use Helpers\DatabaseAdapter;
use Helpers\Logger;

class Users
{
    protected $databaseAdapter;

    public function __construct()
    {
        $this->databaseAdapter = DatabaseAdapter::getInstance();
        $this->logger = Logger::getInstance();
    }

    public function getById($id)
    {
        $this->logger->log(Logger::$INFO, "Getting one user by id from DB");
        $result = $this->databaseAdapter->query("select * from users where id = $id");
        $result = isset($result[0]) ? $result[0] : $result;

        if ($result) {
            $result["phones"] = $this->getPhonesByIdUser($id);
            $result["emails"] = $this->getEmailsByIdUser($id);
        }

        return $result;
    }

    public function getAll()
    {
        $sql = "select * from users";
        $sql = $this->setFilters($sql);
        $this->logger->log(Logger::$INFO, "Getting all users from DB");
        $result = $this->databaseAdapter->query($sql);
        if ($result) {
            foreach ($result as $key => $value) {
                $id = $result[$key]["id"];
                $result[$key]["phones"] = $this->getPhonesByIdUser($id);
                $result[$key]["emails"] = $this->getEmailsByIdUser($id);
            }
        }

        return $result;
    }

    private function setFilters($sql)
    {
        $ids = array();
        $filters = 0;
        $hasFilters =
            isset($_GET["first_name"]) ||
            isset($_GET["surnames"]) ||
            isset($_GET["email"]) ||
            isset($_GET["phone"]);

        if ($hasFilters) {
            $sql.= " where ";
        }
        if (isset($_GET["first_name"])) {
            $firstName = $_GET["first_name"];
            $sql.= " first_name = \"$firstName\"";
            $filters++;
        }
        if (isset($_GET["surnames"])) {
            if ($filters) {
                $sql .= " and ";
            }
            $surnames = $_GET["surnames"];
            $sql.= " surnames = \"$surnames\"";
            $filters++;
        }
        if (isset($_GET["email"]) || isset($_GET["phone"])) {
            if ($filters) {
                $sql .= " and ";
            }
            $sql .= " id in (" . $this->getFilterIds() . ")";
        }
        return $sql;
    }

    private function getFilterIds()
    {
        $arrayIds = array();
        $ids = array();
        if (isset($_GET["email"])) {
            $email = $_GET["email"];
            $results = $this->databaseAdapter->query("select id_user from emails where email = \"$email\"");

            foreach ($results as $result) {
                $ids[] = $result["id_user"];
            }
            $ids = array_unique($ids);
            $arrayIds = $ids;
            $filters++;
        }
        if (isset($_GET["phone"])) {
            $phone = $_GET["phone"];
            $results = $this->databaseAdapter->query("select id_user from phones where phone = \"$phone\"");
            foreach ($results as $result) {
                $ids[] = $result["id_user"];
            }
            $ids = array_unique($ids);
            $arrayIds = array_merge($arrayIds, $ids);
            $filters++;
        }
        return implode(", ", $arrayIds);

    }

    public function create($first_name, $surnames, $phones=array(), $emails=array())
    {
        $statement = "insert into users (first_name, surnames) values (\"$first_name\", \"$surnames\")";
        $this->logger->log(Logger::$INFO, "Inserting users");
        $id = $this->databaseAdapter->exec($statement);

        foreach ($phones as $phone) {
            $statement = "insert into phones (phone, id_user) values (\"$phone\", $id)";
            $this->logger->log(Logger::$INFO, "Inserting users");
            $this->databaseAdapter->exec($statement);
        }

        foreach ($emails as $email) {
            $statement = "insert into emails (email, id_user) values (\"$email\", $id)";
            $this->logger->log(Logger::$INFO, "Inserting users");
            $this->databaseAdapter->exec($statement);
        }

        return true;
    }

    public function update($id, $first_name, $surnames, $phones, $emails)
    {
        $statement = "delete from phones where id_user=$id";
        $this->databaseAdapter->exec($statement);

        $statement = "delete from emails where id_user=$id";
        $this->databaseAdapter->exec($statement);

        $statement = "update users set first_name=\"$first_name\", surnames=\"$surnames\" where id=$id";
        $this->logger->log(Logger::$INFO, "Updating user with id $id");
        $this->databaseAdapter->exec($statement);

        foreach ($phones as $phone) {
            $statement = "insert into phones (phone, id_user) values (\"$phone\", $id)";
            $this->logger->log(Logger::$INFO, "Inserting users");
            $this->databaseAdapter->exec($statement);
        }

        foreach ($emails as $email) {
            $statement = "insert into emails (email, id_user) values (\"$email\", $id)";
            $this->logger->log(Logger::$INFO, "Inserting users");
            $this->databaseAdapter->exec($statement);
        }

        return true;
    }

    public function delete($id)
    {
        $statement = "delete from users where id=$id";
        $this->logger->log(Logger::$INFO, "Deleting user with id $id");
        $this->databaseAdapter->exec($statement);
        return true;
    }

    protected function getPhonesByIdUser($id)
    {
        $sql = "select * from phones where id_user = $id";
        $this->logger->log(Logger::$INFO, "Getting phones for user with id $id");
        return $this->databaseAdapter->query($sql);
    }

    protected function getEmailsByIdUser($id)
    {
        $sql = "select * from emails where id_user = $id";
        $this->logger->log(Logger::$INFO, "Getting emails for user with id $id");
        return $this->databaseAdapter->query($sql);
    }
}
