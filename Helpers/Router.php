<?php
namespace Helpers;

use Helpers\Logger;
use Controllers\PhoneBookController;

class Router
{
    private $requestMethod;

    private static $instance;

    private $controller;

    private function __construct()
    {
        $this->controller = new PhoneBookController();
    }

    public static function getInstance()
    {
        if (self::$instance == null)
        {
          self::$instance = new Router();
        }

        return self::$instance;
    }

    public function resolve()
    {
        $requestMethod = strtolower($_SERVER['REQUEST_METHOD']) . "Resolve";
        $this->$requestMethod();
    }

    private function postResolve()
    {
        if ($_FILES) {
            $this->controller->uploadProfileImageAction();
        } else {
            $this->controller->createAction();
        }
    }

    private function putResolve()
    {
        $this->controller->updateAction();
    }

    private function getResolve()
    {
        $this->controller->retrieveAction();
    }

    private function deleteResolve()
    {
        $this->controller->deleteAction();
    }


    public function responseWithError($error)
    {
        Logger::getInstance()->log(Logger::$ERROR, $error);
        header('HTTP/1.0 500 Internal Server Error');
        echo json_encode(["message" => $error->getMessage()]);
    }
}
