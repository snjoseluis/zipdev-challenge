<?php
namespace Helpers;

use Interfaces\LoggerInterface;

class Logger implements LoggerInterface
{
    public static $EMERGENCY = "emergency";

    public static $ALERT = "alert";

    public static $CRITICAL = "critical";

    public static $ERROR = "error";

    public static $WARNING = "warning";

    public static $NOTICE = "notice";

    public static $INFO = "info";

    public static $DEBUG = "debug";

    private static $instance;

    private function __construct()
    {

    }

    public static function getInstance()
    {
        if (self::$instance == null)
        {
          self::$instance = new Logger();
        }

        return self::$instance;
    }

    public function emergency($message)
    {
        error_log(self::$EMERGENCY . ": " .$message, 0);
    }

    public function alert($message)
    {
        error_log(self::$ALERT . ": " .$message, 0);
    }

    public function critical($message)
    {
        error_log(self::$CRITICAL . ": " .$message, 0);
    }

    public function error($message)
    {
        error_log(self::$ERROR . ": " .$message, 0);
    }

    public function warning($message)
    {
        error_log(self::$WARNING . ": " .$message, 0);
    }

    public function notice($message)
    {
        error_log(self::$NOTICE . ": " .$message, 0);
    }

    public function info($message)
    {
        error_log(self::$INFO . ": " .$message, 0);
    }

    public function debug($message)
    {
        error_log(self::$DEBUG . ": " .$message, 0);
    }

    public function log($level, $message)
    {
        $this->$level($message);
    }
}
