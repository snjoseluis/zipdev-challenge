<?php
namespace Helpers;

use Helpers\Logger;

class DatabaseAdapter
{
    private $user;

    private $password;

    private $db;

    private $host;

    private static $instance;

    private $connection;

    private $logger;

    private function __construct()
    {
        $config = parse_ini_file(__DIR__ . "/../app.ini", true);

        $this->user = $config["database"]["user"];
        $this->password = $config["database"]["password"];
        $this->db = $config["database"]["db"];
        $this->host = $config["database"]["host"];
        $this->connection = new \mysqli($this->host, $this->user, $this->password, $this->db);

        if ($this->connection->connect_error) {
            die($this->connection->connect_errno . ': ' . $this->connection->connect_error);
        }

        $this->logger = Logger::getInstance();
    }

    public static function getInstance()
    {
        if (self::$instance == null)
        {
          self::$instance = new DatabaseAdapter();
        }

        return self::$instance;
    }

    public function query($sql)
    {
        $this->logger->log(Logger::$INFO, "Executing query " . $sql);
        $result = mysqli_query($this->connection, $sql);
        return $result ? mysqli_fetch_all($result, MYSQLI_ASSOC) : array();
    }

    public function exec($sql)
    {
        $this->logger->log(Logger::$INFO, "Executing statement " . $sql);
        $result = mysqli_query($this->connection, $sql);
        if (!$result || $this->connection->error) {
            throw new \Exception($this->connection->error);
        }
        $this->logger->log(Logger::$INFO,"Last affected id " . $this->connection->insert_id);
        return $this->connection->insert_id;
    }
}
