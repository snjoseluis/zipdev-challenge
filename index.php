<?php
namespace App;

$loader = require __DIR__ . '/vendor/autoload.php';

$loader->addPsr4('App\\', __DIR__);

use Helpers\Router;
use Helpers\DatabaseAdapter;
use Helpers\Logger;

$logger = Logger::getInstance();
$logger->log(Logger::$INFO, "Initializing");
$databaseAdapter = DatabaseAdapter::getInstance();
$router = Router::getInstance();
$logger->log(Logger::$INFO, "Resolving route");

try {
    $router->resolve();
} catch (\Exception $e) {
    $router->responseWithError($e);
}


