<?php
namespace Controllers;

use Helpers\Logger;
use Models\Users;


class PhoneBookController
{

    public function createAction()
    {
        Logger::getInstance()->log(Logger::$INFO, "Creating user");

        $users = new Users();
        $data = json_decode(file_get_contents('php://input'), true);

        if (!isset($data["first_name"])) {
            throw new \Exception("Field first_name is required");

        }
        if (!isset($data["surnames"])) {
            throw new \Exception("Field surnames is required");

        }

        $phones = isset($data["phones"]) ? $data["phones"] : array();
        $emails = isset($data["emails"]) ? $data["emails"] : array();

        if ($users->create($data["first_name"], $data["surnames"], $phones, $emails)) {
            header('HTTP/1.0 201 Created');
        }
    }

    public function updateAction()
    {
        Logger::getInstance()->log(Logger::$INFO, "Updating user");
        $data = json_decode(file_get_contents('php://input'), true);

        $users = new Users();

        if (!isset($data["first_name"])) {
            throw new \Exception("Field first_name is required");

        }
        if (!isset($data["surnames"])) {
            throw new \Exception("Field surnames is required");

        }
        if (!isset($data["id"])) {
            throw new \Exception("Field id is required");

        }

        $phones = $data["phones"] ? $data["phones"] : array();
        $emails = $data["emails"] ? $data["emails"] : array();

        if ($users->update($data["id"], $data["first_name"], $data["surnames"], $phones, $emails)) {
            header('HTTP/1.0 200 Ok');
        }
    }

    public function retrieveAction()
    {
        $users = new Users();
        Logger::getInstance()->log(Logger::$INFO, "Retrieving Users");

        if (isset($_GET["id"])) {
            $response = $users->getById($_GET["id"]);

            if (empty($response)) {
                header('HTTP/1.0 404 Not Found');
            } else {
                header('HTTP/1.0 200 Ok');
                header("Content-type:application/json");
                print_r(json_encode($response));
            }
        } else {
            $response = $users->getAll();

            if (empty($response)) {
                header('HTTP/1.0 404 Not Found');
            } else {
                header('HTTP/1.0 200 Ok');
                header("Content-type:application/json");
                print_r(json_encode($response));
            }
        }
    }

    public function deleteAction()
    {
        $users = new Users();
        Logger::getInstance()->log(Logger::$INFO, "Deleting user");
        $data = json_decode(file_get_contents('php://input'), true);

        if (!isset($data["id"])) {
            throw new \Exception("Field id is required");

        }

        if ($users->delete($data["id"])) {
            header('HTTP/1.0 204 No Content');
        }
    }

    public function uploadProfileImageAction()
    {
        if (!isset($_POST["id"])) {
            throw new \Exception("Field id is required");

        }

        $users = new Users();
        $response = $users->getById($_POST["id"]);

        if (empty($response)) {
            header('HTTP/1.0 404 Not Found');
        } else {

            $targetFileName = __DIR__ . "/../img/" . $_POST["id"] . "-" . basename($_FILES["profilePicture"]["name"]);
            $tempName = $_FILES["profilePicture"]["tmp_name"];
            if (!move_uploaded_file($tempName, $targetFileName)) {
                throw new Exception("Unable to upload the file");
            }
            header('HTTP/1.0 200 Ok');
            header("Content-type:application/json");
        }

    }
}
