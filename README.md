# CRUD MVC APP WITHOUT FRAMEWORK
## Getting started

### Requirements

**PHP 7.1.23** - It is the version I have in my computer. I did not test it on 5.6 or previous but it should run since I do not have any new feature coming with PHP 7.
**Composer 1.5.2** - The same, it is the version I have in my local. I am not using any dependancy but I am using its autoloader.

### Installation

- `git clone https://snjoseluis@bitbucket.org/snjoseluis/zipdev-challenge.git`
- `cd zipdev-challenge`
- `composer install`

In case the autoloader is not working and you find those "class not found" errors, run:

- `composer dump-autoload`

You need to take the dump DB from the root and create it in your local. Aditionally, you will need to set up the DB credentials in the `app.ini` file.

Run the project with:

- `php -c php.ini -S 0.0.0.0:3000 index.php`

Note: In Windows, you have to replace `php` with `C:\[path\to\php]\php.exe` plus `-c php.ini -S 0.0.0.0:3000 index.php`

Create, Update and Delete use the body's parameters and Get uses the GET or URL parameters.

## Create

`POST http://localhost/`

The following are the fields needed to create a new user:

```
{
    "first_name": string,
    "surnames": string,
    "phones": array(string), // optional
    "emails": array(string), // optional
}
```

Example:

```
{
    "first_name": "Juan",
    "surnames": "Gomez",
    "phones": [ "6565656565" ],
    "emails": [ "email@mail.com" ]
}
```

Posible responses:

- `500` *An error happened. A param is missing.*
- `201` *Created*

## Retrieve

- `GET http://localhost:3000`
- `GET http://localhost:3000?id=[number]`
- `GET http://localhost:3000?first_name=[string]&surnames=[string]&email=[string]&phone=[string]`

Get all without the id, get one if you pass the id. You can

Posible responses:

- `500` *An error happened. A param is missing*.
- `200` *Ok*
- `404` *Not found*

## Update

`PUT http://localhost:3000`

The following are the fields needed to create a new user:
```
{
    "id": integer
}
```

Example:

```
{
    "id": 24
}
```

Posible responses:

- `500` *An error happened. A param is missing.*
- `200` *Ok*
- `404` *Not found*

## Delete

`DELETE http://localhost:3000`

The following are the fields needed to delete a user:
```
{
    "id": integer
}
```

Example:

```
{
    "id": 24
}
```

Posible responses:

- `500` *An error happened. A param is missing.*
- `204` *No Content*
- `404` *Not found*

## Upload Profile Picture

`POST http://localhost/`

The following are the fields needed to upload a user's profile picture:
```
{
    "id": integer
}
```

Attach a File to the request.

**Notice:** This and the create endpoint are very similar, the lack of a file will make this call to follow the create specifications.

Example:

```
{
    "id": 24
}
```

Posible responses:

- `500` *An error happened. A param is missing.*
- `200` *Ok*
- `404` *Not found*
